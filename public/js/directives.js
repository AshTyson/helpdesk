'use strict';

/* Directives */

//Directive for datepicket from jQuery.UI
//Since DOM manipulation by jquery inside the templates is not possible, need to decalre a directive to do that.
//Throws error on including the directive. Most probably an issue with angular 1.5
angular.module('Ticket.directives', []);
/*.directive('datepicker', function() {
   return function(scope, element, attr) {
       element.datepicker({
           inline: true,
           dateFormat: 'dd.mm.yy',
           onSelect: function(dateText) {
               var modelPath = $(this).attr('ng-model');
               putObject(modelPath, scope, dateText);
               scope.$apply();
           }
       });
   }
});
*/