'use strict';

/* Services */

angular.module('Ticket.services', [])

//This service is used to count and filter the tickets based on a criteria (Solved, Open, Pending, New)
.service('TicketCountService', function () {
	return {
		SolvedTickets: function(input) {
			var output = Array();
			for(var i=0,j=0;i<input.length;i++) {
				if(input[i].ticketStatus=="Solved")
					output[j++] = input[i];
			}
			return output;
		},
		UnsolvedTickets: function(input) {
			var output = Array();
			for(var i=0,j=0;i<input.length;i++) {
				if(input[i].ticketStatus=="Open")
					output[j++] = input[i];
			}
			return output;
		},
		PendingTickets: function(input) {
			var output = Array();
			for(var i=0,j=0;i<input.length;i++) {
				if(input[i].ticketStatus=="Pending")
					output[j++] = input[i];
			}
			return output;
		},
		NewTickets: function(input) {
			var output = Array();
			for(var i=0,j=0;i<input.length;i++) {
				if(input[i].ticketStatus=="New")
					output[j++] = input[i];
			}
			return output;
		}
	};
})

// Service used to search query. It searches on the ticket's subject, description and requester detail.
.service('TicketSearchService', function () {
	return {
		Search: function(input, searchstring) {
			var output = Array();
			for(var i=0,j=0;i<input.length;i++) {
				var t = (input[i].subject + input[i].description + input[i].requesterDetail).toLowerCase();
				if(t.includes(searchstring.toLowerCase()))
					output[j++] = input[i];
			}
			return output;
		}
	};
})