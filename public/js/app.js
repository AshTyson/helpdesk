/*
Author: Ashwin A
Version: 0.0.1
Entry point of the application
*/


/*
Uses modules to import filters, directives, services, controllers.
Other modules imported:
i)ngRoute - Used to seperate views and control logic (MVC pattern).
ii)ngTagsInput - Used to iclude tag based input control.
iii)angularMoment - A moment library used to manipulate time display in the DOM
*/

/*
routeProvider directs views based on the following urls and loads their respective controllers
*/

'use strict';


// Declare app level module which depends on filters, and services
angular.module('Ticket', ['Ticket.filters', 'Ticket.directives', 'Ticket.services','Ticket.controllers','ngRoute', 'ngTagsInput', 'angularMoment']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {controller : function(){ window.location.replace('/login'); }, template : "<div></div>"});
    $routeProvider.when('/dashboard', {templateUrl: 'partial/dashboard', controller: 'dashboardCtrl'});
    $routeProvider.when('/add-ticket', {templateUrl: 'partial/add-ticket', controller: 'addTicketCtrl'});
    $routeProvider.when('/view-ticket', {templateUrl: 'partial/view-ticket', controller: 'viewTicketCtrl'});
    $routeProvider.when('/search', {templateUrl: 'partial/search', controller: 'searchCtrl'});
    $routeProvider.when('/login', {controller : function(){ window.location.replace('/login'); }, template : "<div></div>"});
    $routeProvider.otherwise({redirectTo: '/dashboard'});
    $locationProvider.html5Mode(true);
  }]);