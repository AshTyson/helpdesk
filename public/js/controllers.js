'use strict';

/* Controllers */
angular.module('Ticket.controllers', [])




/* MainController used to initialize the angular-moment module*/

.controller('mainController', function() {
	var vm = this;
	vm.time = new Date();
})




/* DashboardController is used by the dashboard page for redirecting to various pages. */

.controller('dashboardCtrl', ['$scope', '$location', function($scope, $location) {
	$scope.redirect = function(page) {
	switch(page) {
		case "new":
		$location.path('/add-ticket');
		break;
		case "view":
		$location.path('/view-ticket');
		break;
		case "search":
		$location.path('/search');
		break;
		default:
		$location.path('/add-ticket');
		break;
	}
	}
}])





/*ViewController is used by a page that is not developed fully. The purpose of this page is to display the ticket details.
Designs to include socket.io to enable livechat with the customer was also made. But for now this controller is dummy*/

.controller('viewCtrl', ['$scope', '$http','$routeParams', function($scope, $http, $routeParams) {
	$scope.ticket_id = $routeParams.id;
	$scope.ret = "";
	$http.get('/getall').
	success(function(data, status, headers, config) {
	$scope.ret = data;
	console.log(data);
	}).
	error(function(data, status, headers, config) {
	$scope.ret = data;
	});

}])





/*A major controller used to enter the details from the user and save it to the database*/

.controller('addTicketCtrl', ['$scope', '$http', '$location', '$timeout', function($scope, $http, $location, $timeout) {
	
	//$scope.options used to initialize the drop downs

	$scope.options1 = ["General", "Sales", "Marketing", "Administration", "Technical"];
	$scope.options2 = ["Question", "Incident", "Problem", "Task"];
	$scope.options3 = ["Low", "Normal", "High", "Urgent"];
	$scope.options4 = ["New", "Open","Pending", "Solved"];

	$scope.status = false;


	//FormSubmit submits the form to the server and redirects user to the view ticket, where the current status of the ticket can be viewed
	//HTTP POST request to send data.

	$scope.FormSubmit = function() {
	$http.post('/ticket/add', $scope.formData)
		.success(function(data, status, headers, config) {
		$scope.status = true;
		$timeout(function() {
			$location.path('/view?id='+data);
		}, 3000);
		})
		.error(function(data, status, headers, config) {
		console.log('Error!');
		});
	}
}])





/*
The module that is used to display all the tickets and their status.
TicketCountService is custom service to count the tickets based on a category.
*/

.controller('viewTicketCtrl', ['$scope', '$http', "TicketCountService", function($scope, $http, TicketCountService) {

	$scope.propertyName = 'subject';
	$scope.reverse = false;

	$scope.checkAll = function() {
	if ($scope.selectedAll) {
		$scope.selectedAll = true;
	} else {
		$scope.selectedAll = false;
	}
	angular.forEach($scope.Items, function (item) {
		item.Selected = $scope.selectedAll;
	});
	}


	//sortBy function sorts the current results based on the category chosen from the table header.
	//$scope.reverse is used to sort the result in descending order. This can be enabled by pressing the caret button near the headers

	$scope.sortBy = function(propertyName) {
		$scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
		$scope.propertyName = propertyName;
	};


	//$scope.viewdata enables tab view. On each category, the result is filtered and displayed.

	$scope.viewdata = function(input) {
		switch(input) {
			case "all":
			$scope.displayData = $scope.apiData;
			break;
			case "solved":
			$scope.displayData = $scope.solved;
			break;
			case "unsolved":
			$scope.displayData = $scope.unsolved;
			break;
			case "pending":
			$scope.displayData = $scope.pending;
			break;
			case "new":
			$scope.displayData = $scope.newtickets;
			break;
			default:
			$scope.displayData = $scope.apiData;
			break;
		}
	}


	//HTTP GET to fetch data from the server. All the data is fetched and then filtered based on category.

	$http.get('/getall')
	.success(function(data, status, headers, config) {
		$scope.displayData = $scope.apiData = data;
		$scope.solved = TicketCountService.SolvedTickets($scope.apiData);
		$scope.unsolved = TicketCountService.UnsolvedTickets($scope.apiData);
		$scope.pending = TicketCountService.PendingTickets($scope.apiData);
		$scope.newtickets = TicketCountService.NewTickets($scope.apiData);
	})
	.error(function(data, status, headers, config) {
		console.log(data);
	});

}])





/*
SearchController is very important since it searches through the archives of ticket data.
TicketSearchService is a custom service for querying the database.
orderByFilter is a in-built filter accessed through Javascript.
Should include auto auggestion for queries.
*/

.controller('searchCtrl', ['$scope', '$http', "TicketSearchService",'orderByFilter', function($scope, $http, TicketSearchService, orderBy) {

	//Some variables to populate, enable and disable controls in the DOM.
	$scope.tags = [];
	$scope.options1 = ["None", "Question", "Incident", "Problem", "Task"];
	$scope.options2 = ["None", "Low", "Normal", "High", "Urgent"];
	$scope.checkboxModel = {
	value2: false,
	value3: false,
	value4: false
	};

	$scope.clearSort = function() {
	$scope.tsort = {
		c1: false,
		c2: false,
		c3: false,
		c4: false,
		c5: false
	}
	}
	
	$scope.clearFilter = function() {
	$scope.checkboxModel = {
		value2: false,
		value3: false,
		value4: false
	}
	}

	$scope.search = function() {
		if($scope.query!=undefined)
		if($scope.query.replace(/\s/g, "") != "")
			$scope.result = TicketSearchService.Search($scope.displayData, $scope.query);
	}


	//Filters are applied here

	$scope.filter = function() {
		$scope.collection = {
			"date": $scope.cdate,
			"category": $scope.tags,
			"priority": $scope.priority,
			"type": $scope.EType
		};
		$scope.result = $scope.displayData;
	}

	$scope.resetTag = function() {
	if($scope.checkboxModel.value3) {
		$scope.tags = [];
	}
	}

	//Sorting of results done here

	$scope.sortResult = function(category) {
		$scope.result = orderBy($scope.result, category, $scope.revTrue);
	}


	//HTTP GET to fetch data from database
	
	$http.get('/getall')
	.success(function(data, status, headers, config) {
		$scope.displayData = $scope.apiData = data;
	})
	.error(function(data, status, headers, config) {
		console.log(data);
	});

	$scope.propertyName = $scope.apiData;

}])