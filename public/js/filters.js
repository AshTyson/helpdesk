'use strict';

/* Filters */

angular.module('Ticket.filters', []).
	//Custom Filter to filter the current input with tag specified in the search filter.
   filter('tagFilter', function() {

      return function(result, input) {
         if(result==undefined) return true;
         if(input.length<1) return result;

         var findOne = function (haystack, arr) {
            for(var j=0;j<haystack.tags.length;j++) {
               for(var k=0;k<arr.length;k++) {
                  if(haystack.tags[j].text.toLowerCase()==arr[k].text.toLowerCase()) {
                     return true;
                  }
               }
             }
             return false;
         };
         var output = Array();
         var j = 0;
         for(var i=0;i<result.length;i++) {
            if(findOne(result[i],input)) {
               output[j++] = result[i];
            }
         }

         return output;

      }
   }).

	//Custom Filter to filter the current input with date specified in the search filter.(Not fully developed)
	//Uses momentJS for date-time manipulation
	filter('dateFilter', function() {

      return function(result, input) {

         if(result==true) return true;

         var output = Array();
         var j = 0;
         for(var i=0;i<result.length;i++) {
            console.log(moment(input, "day"), moment(result[i].createdOn, "day"));
            if(moment(result[i].createdOn).isSame(moment(input), "day")) {
               output[j++] = result[i];
            }
         }

         return result;

      }
   })
