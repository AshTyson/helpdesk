
/**
 * Module dependencies
 */
var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  favicon = require('serve-favicon');
  http = require('http'),
  bodyParser = require('body-parser'),
  path = require('path');

var moment = require('moment');
var nodemailer = require('nodemailer');

var app = module.exports = express();


var ticket = require('./routes/ticket');

/**
* Configuration
*/
app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// all environments
app.set('port', process.env.PORT || 9000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

app.use('/ticket', ticket);

// Routes
app.get('/getall', api.getall);
app.get('/login', routes.login);
app.get('/', routes.index);
app.get('/partial/:name', routes.partial);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

/**
* Start Server
*/

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
