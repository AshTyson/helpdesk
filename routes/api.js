/*
 * Serve JSON to our AngularJS client
 */

var db = require('monk')('localhost/ticketsystem');
var tickets = db.get('tickets');

exports.getall = function(req, res, next) {
	tickets.find({}).then(function (docs) {
		res.status(200).send(docs);
	});
};
