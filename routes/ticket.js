var express = require('express');
var router = express();
var moment = require('moment');
var Ticket = require('../models/ticketSchema');

var db = require('monk')('localhost/ticketsystem');
var tickets = db.get('tickets');

router.post('/add', function(req, res, next) {

    var newTicket = new Ticket();

    newTicket.requesterDetail = req.body.requesterDetail;
    newTicket.assigneeType = req.body.assigneeType;
    newTicket.contactEmail = req.body.contactEmail;
    newTicket.ticketType = req.body.ticketType;
    newTicket.ticketPriority = req.body.ticketPriority;
    newTicket.tags = req.body.tags;
    newTicket.subject = req.body.subject;
    newTicket.description = req.body.description;
    newTicket.ticketStatus = req.body.ticketStatus;
    newTicket.createdOn = moment().toDate();

    tickets.insert(newTicket, function(err, doc) {	
    	if(err) throw err;
		res.status(200).send(doc._id);
    });
});

module.exports = router;